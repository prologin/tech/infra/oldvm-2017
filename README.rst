Prologin VM
===========

VM setup
--------

- Install a VM with a recent version of Archlinux (https://www.archlinux.org/).
  You should have at least 1G of ram for the VM. You can try with less, but you
  probably won't be able to compile some packages and will have to do it
  somewhere else.
- Generate a root SSH key::

    ssh-keygen
    cat ~/.ssh/id_rsa.pub

- Add this key to the *Deployment Keys* of the ``problems`` repository
  (https://bitbucket.org/prologin/problems/admin/deploy-keys)

Installation
------------

Install git and openssh::

    pacman -Sy git openssh

Clone the repository::

    git clone https://bitbucket.org/prologin/vm
    cd vm

Install the sandbox::

    ./sandbox.sh install

Check if all the languages work::

    ./sandbox.sh check

Check that the bridge is running::

    curl http://localhost:8080/
    # Should output "Bridge working."

Firewall
--------

During the installation, a systemd service ``vm-firewall`` has automatically be
added to your services. If you want to enable and start it::

    systemctl enable vm-firewall
    systemctl start vm-firewall

*WARNING*: After being started, this service will drop any outcoming
connections that are not coming from the root user.

Problems Update
---------------

To update the problems directory every 3 minutes so that the problems are
always up to date, you can use this timer::

    systemctl enable vm-problems-update.timer
    systemctl start vm-problems-update.timer

Uninstall
---------

Do *NOT* try to rm -rf sandbox. A procfs is mounted inside, and is added in the
fstab. Plus, you will keep some useless users in your system. Use::

    ./sandbox.sh remove


Internals
---------

To check a source code, call check/run.sh. This:

- detects the language based on the file extension
- compiles the code for compiled languages, outside the sandbox,
  using the makefile "check/Compile". This is where you should set
  compiler flags if needed.
- sets the memory & time constraints based on the chosen language
- calls big_brother, which chroots(), executes the code and monitors it.
  The time limit is enforced by big_brother and the memory limit is enforced
  by a shared library we preload and which spawns a thread which periodically
  checks memory usage using /proc on GNU/Linux.
- produces the XML output.
