#!/bin/sh

RED='\e[1;31m'
GREEN='\e[1;32m'
NC='\e[0m'

PREFIX=/var/prologin

CNAME=check
DNAME=deps
PNAME=problems
SNAME=sandbox

CPATH="$PREFIX/$CNAME"
DPATH="$PREFIX/$DNAME"
PPATH="$PREFIX/$PNAME"
SPATH="$PREFIX/$SNAME"

NB_USERS=20

WWW_USER=www
NOPRIV_USER=sandboxuser
# Warning: this is also harcoded in check/big_brother.c:CLIENT_USER_FMT
SANDBOX_USER_PREFIX=sandboxnobody

set -e

if [ "$( id -u )" -ne 0 ]; then
    echo "You must be root!" >&2
    exit 1
fi

cd "$(dirname "$0")"

case $1 in
    install|remove|check|update)
        ACTION=$1
        shift
        ;;
    *)
        echo "usage: $0 (install|remove|update|check)"
        exit 1
        ;;
esac

create_user_group()
{
    opts=${2:-"-M -d /dev/null"}
    if ! id "$1" > /dev/null 2>&1; then
        groupadd "$1"
        useradd $opts -g "$1" -s /bin/false "$1"
    fi
}

delete_user_group()
{
    if id "$1" > /dev/null 2>&1; then
        userdel "$1"
    fi
}

alias noprivdo="sudo -u \"$NOPRIV_USER\" -g \"$NOPRIV_USER\" -- "

sandbox_install() {
    mkdir -p "$PREFIX"

    cp -r "$CNAME" "$PREFIX"
    cp -r "$DNAME" "$PREFIX"

    if [ -d "$PPATH" ]; then
        git -C "$PPATH" pull --rebase
    else
        git clone --depth=1 git@bitbucket.org:prologin/problems.git "$PPATH"
    fi

    cp services/* /etc/systemd/system/

    mkdir -p "$SPATH"

    pacman --color=never --needed --noconfirm -S $(<$DPATH/host)
    if [ ! -f "$SPATH/etc/passwd" ]; then
        pacstrap -d "$SPATH" base base-devel || exit 1
    fi

    # Yaourt install
    create_user_group "$NOPRIV_USER" -m
    cat >/etc/sudoers.d/user-makepkg <<EOF
$NOPRIV_USER ALL=NOPASSWD: /usr/bin/pacman
EOF

    if ! pacman -Q yaourt >/dev/null 2>&1
    then
        noprivdo sh <<"EOC"
mkdir -p /tmp/yaourt-build
cd /tmp/yaourt-build
curl -O https://aur.archlinux.org/cgit/aur.git/snapshot/package-query.tar.gz
tar -xvf package-query.tar.gz
( cd package-query && makepkg --nocolor --needed --noconfirm -sri )
curl -O https://aur.archlinux.org/cgit/aur.git/snapshot/yaourt.tar.gz
tar -xvf yaourt.tar.gz
( cd yaourt && makepkg --nocolor --needed --noconfirm -sri )
rm -r /tmp/yaourt-build
EOC
    fi

    # Install sandbox deps in host & sandbox
    noprivdo yaourt --cachedir /var/cache/pacman/pkg --nocolor --needed --noconfirm \
                    -S $(<$DPATH/sandbox)
    noprivdo yaourt --cachedir /var/cache/pacman/pkg --nocolor --needed --noconfirm \
                    --root "$SPATH" \
                    -S $(<$DPATH/sandbox)

    test -c "$SPATH/dev/random" || mknod "$SPATH/dev/random" c 1 8
    test -c "$SPATH/dev/urandom" || mknod "$SPATH/dev/urandom" c 1 9

    if ! grep -q "proc $SPATH/proc proc defaults 0 0" /etc/fstab; then
        echo "proc $SPATH/proc proc defaults 0 0" >> /etc/fstab
    fi

    mountpoint -q "$SPATH/proc" || mount "$SPATH/proc"

    create_user_group "$WWW_USER"
    for i in $( seq 1 $NB_USERS ); do
        create_user_group "${SANDBOX_USER_PREFIX}$i"
    done

    BIG_BROTHER="$SPATH/usr/bin/big_brother"
    gcc -Wall -Wextra -O2 -std=c99 -lpthread "$CPATH/big_brother.c" \
        -o "$BIG_BROTHER"
    chmod +s "$BIG_BROTHER"
}


sandbox_update() {
    noprivdo yaourt --cachedir /var/cache/pacman/pkg --nocolor --needed --noconfirm \
                    --root "$SPATH" \
                    -Syu

    git -C "$PPATH" pull --rebase
}


sandbox_remove() {
    for i in $( seq 1 $NB_USERS ); do
        delete_user_group "${SANDBOX_USER_PREFIX}$i"
    done
    delete_user_group "$WWW_USER"
    delete_user_group "$NOPRIV_USER"

    mountpoint -q "$SPATH/proc" && umount "$SPATH/proc"

    if grep -q "proc $SPATH/proc proc defaults 0 0" /etc/fstab; then
        sed -i "\#proc $SPATH/proc proc defaults 0 0#d" /etc/fstab
    fi

    rm -rf "$SPATH"
}


sandbox_check() {
    languages="py py3 php adb cs fs scm ml c cpp pas hs js bf pl lua vb java"
    languages=${1:-$languages}
    tests_tot=0
    tests_passed=0
    umask 0000
    tmp=$( mktemp )

    for ext in $languages; do
        FILE="$PPATH/test_vm/memlimit/ref.$ext"

        "$CPATH/run.sh" test_vm memlimit "$FILE" 10000 10000 > "$tmp"

        if grep -q 'error="0"' "$tmp"; then
            echo -e "${GREEN}============== Test OK: $FILE ==============${NC}"
            tests_passed=$(( tests_passed + 1 ))
        else
            echo -e "${RED}============ Test FAILED: $FILE ============${NC}"
        fi
        tests_tot=$(( tests_tot + 1 ))
        cat "$tmp"
    done

    echo
    echo
    echo -e -n "==> Tests passed $tests_passed/$tests_tot: "
    if [ "$tests_tot" = "$tests_passed" ]; then
        echo -e "${GREEN}OK!${NC}"
    else
        echo -e "${RED}FAILED!${NC}"
    fi
}

sandbox_"$ACTION" $*
