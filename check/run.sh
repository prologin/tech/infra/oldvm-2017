#!/bin/bash

# some variables.
#
PREFIX=/var/prologin
CHECK_ROOT="$PREFIX/check"
PROBLEMS_ROOT="$PREFIX/problems"
SANDBOX_ROOT="$PREFIX/sandbox"
LIMIT_SIZE=40000
ERROR_LIMIT=1

#
# all output should be piped to the following function
#

function encode()
{
    perl -e 'while (<>) {
  @chars = split //;
  foreach $c (@chars) {
    if (((ord $c) < 32) && ($c !~ /[\t\n\r]/)) {
      print "\\".(ord $c)
    }
    elsif ($c eq "&") {
      print "&amp;"
    }
    elsif ($c eq "\"") {
      print "&quot;"
    }
    elsif ($c eq "<") {
      print "&lt;"
    }
    elsif ($c eq ">") {
      print "&gt;"
    }
    else {
      print $c
    }
  }
}' 2> /dev/null
}

#
# gets a problem property (getprop 'propname' file.props)
#

function getprop()
{
    grep "^\s*$1:" "$2" | cut -d ':' -f 2- | sed 's/^\s*//g' | sed 's/\s*$//g'
}

#
# arguments check.
#

if [ "${#}" -lt 3 ]; then
    echo "usage: ${0} challenge-id problem-id source-to-check [time_limit] [mem_limit]"
    exit 1
fi

echo "<?xml version='1.0' standalone='yes'?>"
echo -e "<submission>"

#
# aliases.
#

CHALLENGE=${1}
PROBLEM=${2}
SOURCE=${3}

#
# check challenge & problem validity.
#

if [ ! -d "${PROBLEMS_ROOT}/${CHALLENGE}" ]; then
    echo -e "<error>invalid challenge ${CHALLENGE}</error>"
    echo -e "</submission>"
    exit 1
fi

if [ ! -d "${PROBLEMS_ROOT}/${CHALLENGE}/${PROBLEM}" ]; then
    echo -e "<error>invalid problem ${PROBLEM}</error>"
    echo -e "</submission>"
    exit 1
fi

if [ ! -f "${SOURCE}" ]; then
    echo -e "<error>no such file ${SOURCE}</error>"
    echo -e "</submission>"
    exit 1
fi

PROBLEM_ROOT="${PROBLEMS_ROOT}"/"${CHALLENGE}"/"${PROBLEM}"

#
# check if a custom check script is defined
#

if [ ! -f "${PROBLEM_ROOT}"/problem.props ]; then
    echo -e "<error>problem.props is missing</error>"
    echo -e "</submission>"
    exit 1
fi

PROBLEM_PROPS="${PROBLEM_ROOT}"/problem.props
CUSTOM_CHECK=$( getprop 'custom-check' "${PROBLEM_PROPS}" )

if [ -n "${CUSTOM_CHECK}" ]; then
    if [ -f "${PROBLEM_ROOT}"/"${CUSTOM_CHECK}" ]; then
        if [ ! -x "${PROBLEM_ROOT}"/"${CUSTOM_CHECK}" ]; then
            echo -e "<error>custom check script ${CUSTOM_CHECK} is not +x</error>"
            echo -e "</submission>"
            exit 1
        fi
    else
        echo -e "<error>no such custom check script${CUSTOM_CHECK}</error>"
        echo -e "</submission>"
        exit 1
    fi
fi

#
# Constraints
#

if [ "${#}" -ge 4 ]; then
    TIME_CONSTRAINT=${4}
else
    TIME_CONSTRAINT=$( getprop 'time' "${PROBLEM_PROPS}" )
fi

if [ "${#}" -ge 5 ]; then
    MEMORY_CONSTRAINT=${5}
else
    MEMORY_CONSTRAINT=$( getprop 'mem' "${PROBLEM_PROPS}" )
fi

if [ -z "${MEMORY_CONSTRAINT}" -o -z "${TIME_CONSTRAINT}" ]; then
    echo -e "<error>some constraints are missing</error>"
    echo -e "</submission>"
fi


#
# compute the language
#

SUFFIX=""
case "${SOURCE}" in
    *.adb)
        MEMORY_CONSTRAINT=$((4096 + MEMORY_CONSTRAINT))
        SUFFIX=".adb"
        LANG="lang-ada";;
    *.bf)
        TIME_CONSTRAINT=$((TIME_CONSTRAINT * 8))
        MEMORY_CONSTRAINT=$((50000 + MEMORY_CONSTRAINT * 10))
        LANG="lang-brainfuck";;
    *.c)
        MEMORY_CONSTRAINT=$((1024 + MEMORY_CONSTRAINT))
        LANG="lang-c";;
    *.cc|*.cpp)
        MEMORY_CONSTRAINT=$((3000 + MEMORY_CONSTRAINT))
        LANG="lang-cc";;
    *.cs)
        TIME_CONSTRAINT=$((TIME_CONSTRAINT + 38))
        MEMORY_CONSTRAINT=$((16384 + MEMORY_CONSTRAINT))
        SUFFIX=".exe"
        LANG="lang-csharp";;
    *.fs)
        MEMORY_CONSTRAINT=$((17408 + MEMORY_CONSTRAINT))
        SUFFIX=".exe"
        LANG="lang-fsharp";;
    *.hs)
        TIME_CONSTRAINT=$((1024 + TIME_CONSTRAINT * 4))
        MEMORY_CONSTRAINT=$((15000 + MEMORY_CONSTRAINT * 5))
        LANG="lang-hs";;
    *.java)
        TIME_CONSTRAINT=$((36000 + TIME_CONSTRAINT * 4))
        MEMORY_CONSTRAINT=$((22000 + MEMORY_CONSTRAINT * 5))
        LANG="lang-java";;
    *.lua)
        TIME_CONSTRAINT=$((TIME_CONSTRAINT * 10))
        MEMORY_CONSTRAINT=$((5000 + MEMORY_CONSTRAINT))
        SUFFIX=".lua"
        LANG="lang-lua";;
    *.ml)
        MEMORY_CONSTRAINT=$((1024 + MEMORY_CONSTRAINT * 2))
        LANG="lang-ocaml";;
    *.pas)
        MEMORY_CONSTRAINT=$((1024 + MEMORY_CONSTRAINT))
        LANG="lang-pas";;
    *.php)
        TIME_CONSTRAINT=$((TIME_CONSTRAINT * 8))
        MEMORY_CONSTRAINT=$((36384 + MEMORY_CONSTRAINT * 5))
        SUFFIX=".php"
        LANG="lang-php";;
    *.pl)
        TIME_CONSTRAINT=$((TIME_CONSTRAINT * 10))
        MEMORY_CONSTRAINT=$((5000 + MEMORY_CONSTRAINT))
        SUFFIX=".pl"
        LANG="lang-perl";;
    *.py)
        TIME_CONSTRAINT=$((TIME_CONSTRAINT * 15))
        MEMORY_CONSTRAINT=$((7500 + MEMORY_CONSTRAINT * 5))
        SUFFIX=".py"
        LANG="lang-python";;
    *.py3)
        TIME_CONSTRAINT=$((TIME_CONSTRAINT * 15))
        MEMORY_CONSTRAINT=$((7500 + MEMORY_CONSTRAINT * 5))
        SUFFIX=".py3"
        LANG="lang-python3";;
    *.scm)
        TIME_CONSTRAINT=$((TIME_CONSTRAINT * 3))
        MEMORY_CONSTRAINT=$((36384 + MEMORY_CONSTRAINT))
        SUFFIX=".scm"
        LANG="lang-scheme";;
    *.js)
        TIME_CONSTRAINT=$((TIME_CONSTRAINT * 5))
        MEMORY_CONSTRAINT=$((100000 + MEMORY_CONSTRAINT * 5))
        SUFFIX=".js"
        LANG="lang-javascript";;
    *.vb)
        MEMORY_CONSTRAINT=$((16384 + MEMORY_CONSTRAINT))
        TIME_CONSTRAINT=$((TIME_CONSTRAINT * 2))
        SUFFIX=".exe"
        LANG="lang-vb";;
    *)
        echo -e "\t<error>unrecognized source language</error>\n</submission>"
        exit 1
esac

MEMORY_CONSTRAINT=$((MEMORY_CONSTRAINT * 2))

OUTPUT_DIR=$( mktemp -d -p "${SANDBOX_ROOT}"/tmp )
OUTPUT=$( mktemp -p "$OUTPUT_DIR" --suffix="$SUFFIX" tmpXXXXXXXXXX )
OUTPUT_EXIT=$( mktemp -p "${OUTPUT_DIR}" )
chmod a=wx "$OUTPUT_DIR"

echo -e "\t<language>${LANG}</language>"

#
# compile.
#

echo -e -n  "\t<compilation>"
make -f "${CHECK_ROOT}/Compile" "${LANG}" SOURCE="${SOURCE}" \
    OUTPUT="${OUTPUT}" INCLUDES="${PROBLEM_ROOT}"            \
    MONO_SHARED_DIR="${SANDBOX_ROOT}" > "${OUTPUT_EXIT}"
ret=$?

if [ "$ret" -ne 0 ]; then
    cut -b 1-"${LIMIT_SIZE}" "$OUTPUT_EXIT" | encode
    echo -e "Erreur de compilation</compilation>\n</submission>"
    chmod +r "${OUTPUT_DIR}" && rm -rf "${OUTPUT_DIR}"
    exit 1
fi

chmod a+rxw "${OUTPUT}"

encode < "${OUTPUT_EXIT}"
rm -f "${OUTPUT_EXIT}"

echo -e "\t</compilation>"

echo -e "\t<time>${TIME_CONSTRAINT}</time>"
echo -e "\t<memory>${MEMORY_CONSTRAINT}</memory>"

#
# run testsuite.
#

OUTPUT_NEWROOT=/tmp/"$( basename "${OUTPUT_DIR}" )/$( basename "${OUTPUT}" )"
STDOUT=$( mktemp -p "${OUTPUT_DIR}" )
DEBUG=$( mktemp -p "${OUTPUT_DIR}" )

ERROR_COUNT=0

PERFORMANCE=$( getprop 'performance' "${PROBLEM_PROPS}" | tr ' ' '\n' )
HIDDEN=$( getprop 'hidden' "${PROBLEM_PROPS}" | tr ' ' '\n' )

for TEST_IN in "${PROBLEM_ROOT}"/*.in; do
    TEST=$( basename "${TEST_IN}" '.in' )
    TEST_OUT="${PROBLEM_ROOT}/${TEST}.out"

    ! ( echo "$PERFORMANCE" | grep -q "^$TEST$" )
    ISPERF=$?

    ! ( echo "$HIDDEN" | grep -q "^$TEST$" )
    ISHIDDEN=$?

    echo -e "\t<test id=\"${TEST}\" performance=\"${ISPERF}\" hidden=\"${ISHIDDEN}\">"

    chmod -R a=rwx "$OUTPUT_DIR"
    chmod a=wx "$OUTPUT_DIR"

    pushd "${SANDBOX_ROOT}" > /dev/null

    case "${SOURCE}" in
        *.cs)
            COMMAND="/usr/bin/mono $OUTPUT_NEWROOT";;
        *.fs)
            COMMAND="/usr/bin/mono $OUTPUT_NEWROOT";;
        *.java)
            CLASSPATH=${OUTPUT_NEWROOT%/*}
            CLASSFILE=$( basename "$OUTPUT_NEWROOT" .class )
            HEAP_CONSTRAINT=$(((MEMORY_CONSTRAINT - 20000) * 2 / 3 ))
            COMMAND="/usr/bin/java -Xmx${HEAP_CONSTRAINT}k -cp $CLASSPATH $CLASSFILE"
            ;;
        *.lua)
            COMMAND="/usr/bin/luajit $OUTPUT_NEWROOT";;
        *.php)
            COMMAND="/usr/bin/php $OUTPUT_NEWROOT";;
        *.pl)
            COMMAND="/usr/bin/perl $OUTPUT_NEWROOT";;
        *.py)
            COMMAND="/usr/bin/python2 -S $OUTPUT_NEWROOT";;
        *.py3)
            COMMAND="/usr/bin/python3 -S $OUTPUT_NEWROOT";;
        *.scm)
            COMMAND="/usr/bin/gsi $OUTPUT_NEWROOT";;
        *.js)
            COMMAND="/usr/bin/node $OUTPUT_NEWROOT";;
        *.vb)
            COMMAND="/usr/bin/mono $OUTPUT_NEWROOT";;
        *)
            COMMAND="$OUTPUT_NEWROOT";;
    esac

    LIMIT_OUTPUT_FILE_SIZE=$(( 5 * 1024 * 1024 )) # 5 Mio

    # ${COMMAND} must be expanded without quotes
    usr/bin/big_brother swapon "${TIME_CONSTRAINT}" "${MEMORY_CONSTRAINT}" \
        ${COMMAND} < "${TEST_IN}" \
        > >(head -c "$LIMIT_OUTPUT_FILE_SIZE" > "${STDOUT}" ) \
        2> >(head -c "$LIMIT_OUTPUT_FILE_SIZE" > "${DEBUG}" )

    case "${?}" in
    0)
        if [ -n "${CUSTOM_CHECK}" ]; then
            DIFF_CODE=$( "${PROBLEM_ROOT}"/"${CUSTOM_CHECK}" "${STDOUT}" \
                            "${TEST_IN}" "${TEST_OUT}" )
            if [[ $DIFF_CODE = *[![:digit:]]* ]]; then
                echo -e -n "\t\t<summary error=\"1\">"
                echo -e -n "Custom checker returned a non-numerical value: ${DIFF_CODE}"
                echo -e "</summary>\n\t</test>\n</submission>"
                exit 1
            fi
        else
            diff -u -w -b "${STDOUT}" "${TEST_OUT}" > /dev/null 2>&1
            DIFF_CODE=$?
        fi

        if [ "${DIFF_CODE}" -ne 0 ]; then
        ERROR_COUNT=$((ERROR_COUNT + 1))
        if [ "${ERROR_COUNT}" -le "${ERROR_LIMIT}" ]; then
            #
            # debug output.
            #
            if [ -s "${DEBUG}" ]; then
            echo -ne "\t\t<debug>"
            cut -b 1-${LIMIT_SIZE} "$DEBUG" | encode
            echo -e "</debug>"
            fi

            #
            # detailed diff.
            #
            echo -e "\t\t<details>"
            REF_SIZE=$( stat -c '%s' "${TEST_OUT}" )
            OUT_SIZE=$( stat -c '%s' "${STDOUT}" )
            if [ \( "${REF_SIZE}" -gt "${LIMIT_SIZE}" \) -o \
            \( "${OUT_SIZE}" -gt "${LIMIT_SIZE}" \) ]; then
            echo -e "\t\t\t<diff>"
            diff -u "${STDOUT}" "${TEST_OUT}" | cut -b 1-${LIMIT_SIZE} | encode
            echo -e "\t\t\t</diff>"
            else
            echo -ne "\t\t\t<program>"
            cut -b 1-${LIMIT_SIZE} "$STDOUT" | encode
            echo -ne "</program>\n\t\t\t<ref>"
            encode < "$TEST_OUT"
            echo -e "</ref>"
            fi
            echo -e "\t\t</details>"
        fi
        echo -e -n "\t\t<summary error=\"1\">"
        echo -e -n "Sorties differentes." | encode
        echo -e "</summary>"
        else
        echo -e -n "\t\t<summary error=\"0\">"
        echo -e -n "Test reussi."
        echo '</summary>'
        fi
        ;;
    42)
        #
        # debug output.
        #
        ERROR_COUNT=$((ERROR_COUNT + 1))
        if [ "${ERROR_COUNT}" -le "${ERROR_LIMIT}" ]; then
        if [ -s "${DEBUG}" ]; then
            echo -e "<debug>"
            cut -b 1-${LIMIT_SIZE} "$DEBUG" | encode
            echo
            echo "stdout:"
            cut -b 1-${LIMIT_SIZE} "$STDOUT" | encode
            echo -e "</debug>"
        fi
        fi
        echo -e -n "\t\t<summary error=\"1\">"
        echo -e -n "Votre programme a declenche une erreur fatale ou n'a pas respecte les contraintes de temps et/ou de memoire." | encode
        echo -e "</summary>"
        ;;
    *)
        echo -e "\t\t<error>script error ${?}</error>\n"
            echo -e -n "\t\t<summary error=\"1\">"
            echo -e -n "script error ${?}"
            echo -e "</summary>\n</test>\n</submission>"
        echo -ne "\t\"<debug>"
        echo "stdout:"
        cut -b 1-${LIMIT_SIZE} "$STDOUT" | encode
        echo
        echo "stderr:"
        cut -b 1-${LIMIT_SIZE} "$DEBUG" | encode
        echo -e "</debug>"

        exit 1
        ;;
    esac

    popd > /dev/null

    echo -e "\t</test>"
done

echo -e "</submission>"

#
# finished.
#

chmod +r "${OUTPUT_DIR}" && rm -rf "${OUTPUT_DIR}"
