#define _GNU_SOURCE
#define _ISOC99_SOURCE

#include <errno.h>
#include <fcntl.h>
#include <grp.h>
#include <libgen.h>
#include <pthread.h>
#include <pwd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>

// Check every N micro-seconds for memory/time
#define POLL_TIME_INTERVAL    (20 * 1000)
#define POLL_MEM_INTERVAL    (50 * 1000)

// Factor allowing a program to run MAX_REALTIME*ALLOWED if
// user-time is not reached
#define MAX_REALTIME        (3)

// User under which the big_brother program will run
// Whereas the student's code will run under nobody:nogroup
#define CLIENT_USER_FMT        "sandboxnobody%d"
#define CLIENT_USER_MAX        20 // 1 -> 20

#define UNUSED    __attribute__((unused))
#define ASSERT(Exp, Msg)                        \
  do {                                    \
    if (!(Exp))                                \
    {                                    \
      fprintf(stderr, "FATAL ERROR: " Msg ": %s\n", strerror(errno));    \
      exit(EXIT_FAILURE);                        \
    }                                    \
  } while(0)

// Thread (memory watchdog) arguments
typedef struct
{
  pid_t    child_pid;
  int    mem_limit;
}    t_mem_watchdog_arg;

// "Memory errors" return value of the watchdog thread
typedef enum
{
  MEM_OK,
  MEM_OVERFLOW
}    e_mem_status;

// Drop privileges
static int drop_privs(uid_t uid, gid_t gid)
{
  if (setgroups(1, &gid) || setgid(gid) || setuid(uid))
    return -1;

  return 0;
}

// Child
static void child_exec(int swapoff, char** full_cmd)
{
  struct rlimit lim;

  // Disable memory swapping
  if (swapoff)
    ASSERT(mlockall(MCL_CURRENT | MCL_FUTURE) != -1, "cannot mlockall");

  // Disable coredumps
  ASSERT(getrlimit(RLIMIT_CORE, &lim) != -1, "cannot getrlimit: core");
  lim.rlim_cur = 0;
  ASSERT(setrlimit(RLIMIT_CORE, &lim) != -1, "cannot setrlimit: core");

  // Set maximum number of thread
  ASSERT(getrlimit(RLIMIT_NPROC, &lim) != -1, "cannot getrlimit: nproc");
  lim.rlim_cur = 40;
  ASSERT(setrlimit(RLIMIT_NPROC, &lim) != -1, "cannot setrlimit: nproc");

  // Set maximum memory usage to 200MB maximum (hard limit)
  /* ASSERT(getrlimit(RLIMIT_AS, &lim) != -1, "cannot getrlimit: as"); */
  /* lim.rlim_cur = (1024 * 1024) * 200; */
  /* ASSERT(setrlimit(RLIMIT_AS, &lim) != -1, "cannot setrlimit: as"); */

  /* ASSERT(getrlimit(RLIMIT_FSIZE, &lim) != -1, "cannot getrlimit"); */
  /* lim.rlim_cur = 0; */
  /* ASSERT(setrlimit(RLIMIT_FSIZE, &lim) != -1, "cannot setrlimit"); */

  /* ASSERT(getrlimit(RLIMIT_NOFILE, &lim) != -1, "cannot getrlimit"); */
  /* lim.rlim_cur = 10; */
  /* ASSERT(setrlimit(RLIMIT_NOFILE, &lim) != -1, "cannot setrlimit"); */

  // Execute the correct binary and pass it any arguments left
  char* empty_env[] = { NULL };
  ASSERT(execve(full_cmd[0], full_cmd, empty_env) != -1, "cannot execve");
}


// Return the realtime used by a process
static int get_real_time(char* stat_path)
{
  FILE* h = fopen(stat_path, "r");
  char token[200] = { 0 };
  int i;
  int utime;
  int cutime;

  if (h == NULL)
    return (-1);

#define GET_TOKENS(N, Var) {                    \
    for (i = 0; i < N && fscanf(h, "%200s", token) == 1; i++)    \
      ;                                \
    if (i == N)                            \
      Var = atoi(token);                    \
    else                            \
    {                                \
      return (-1);                        \
      fclose(h);                        \
    }                                \
  }

  GET_TOKENS(14, utime);
  GET_TOKENS(02, cutime);

  fclose(h);
  return (utime + cutime) * sysconf(_SC_CLK_TCK) * 100;
}

// Monitor
static int monitor_time(pid_t child_pid, pthread_t* watchdog_thread,
            int time_allowed, int mem_limit)
{
  int status;
  int ret;
  int total = 0;
  char stat_path[100];
  int timeout = 0;

  snprintf(stat_path, 100, "/proc/%d/stat", child_pid);
  while (!(ret = waitpid(child_pid, &status, WNOHANG)))
  {
    usleep(POLL_TIME_INTERVAL);
    total += POLL_TIME_INTERVAL;

    int user_time = get_real_time(stat_path);

    // Timeout
    if (total > MAX_REALTIME * time_allowed ||
    (user_time != -1 && user_time > time_allowed))
    {
      timeout = 1;
      kill(child_pid, SIGKILL);
    }
  }

  // Join the thread
  void* retval = 0;
  pthread_join(*watchdog_thread, &retval);
  e_mem_status ret_mem = (e_mem_status) retval;

  // Get memory peak usage
  struct rusage usage;
  int mem_used; // in Bytes
  ASSERT(getrusage(RUSAGE_CHILDREN, &usage) != -1,
     "Cannot get memory usage of the process");
  mem_used = usage.ru_maxrss * 1024;

  // Check memory: realtime & peak usage
  if (mem_used > mem_limit || ret_mem == MEM_OVERFLOW)
  {
    fprintf(stderr, "Limite de mémoire dépassee\n");
    return 42;
  }

  // Stopped after a timeout
  if (timeout)
  {
    fprintf(stderr, "Limite de temps dépassee\n");
    return 42;
  }

  // Standard exit
  if (WIFEXITED(status))
  {
    return EXIT_SUCCESS;
  }
  // Killed by status
  else
  {
    int signal_code = status & 0x7F;
    fprintf(stderr,
        "Arrêt anormal du processus, signal ayant mis fin à son execution : "
        "\"%s\" (%d).\n", strsignal(signal_code), signal_code);
    return 42;
  }
}

// Write string on stderr
static void writes(char* s)
{
  ASSERT(write(STDERR_FILENO, s, strlen(s)) != -1, "cannot write");
}

// Get the RSS of a process
static int get_rss(char* statm_path)
{
  FILE* h = fopen(statm_path, "r");
  int total_size;
  int rss;

  // "<total_program_size> <resident_set_size>"
  if (h == NULL ||
      fscanf(h, "%d %d", &total_size, &rss) == -1)
    return (-1);

  fclose(h);

  return (rss);
}

// Watchdog thread
static void* mem_watchdog(void* args)
{
  t_mem_watchdog_arg* args_typed = args;
  int mem_limit = args_typed->mem_limit;
  int child_pid = args_typed->child_pid;
  char statm_path[100];

  free(args);
  snprintf(statm_path, 100, "/proc/%d/statm", child_pid);

  for (int rss = 0; rss < mem_limit; )
  {
    rss = get_rss(statm_path);
    if (rss == -1)
      pthread_exit((void*) MEM_OK);

    rss *= getpagesize();
    usleep(POLL_MEM_INTERVAL);
  }

  // The program used too much memory
  kill(child_pid, SIGKILL);
  pthread_exit((void*) MEM_OVERFLOW);
}

// Start thread
static void run_mem_watchdog(pid_t child_pid, int mem_limit,
                 pthread_t* watchdog_thread)
{
  t_mem_watchdog_arg* arg_thread;

  if ((arg_thread = malloc(sizeof(*arg_thread))) == NULL)
  {
    writes("Could not allocate memory for the thread\n");
    exit(4);
  }

  arg_thread->child_pid = child_pid;
  arg_thread->mem_limit = mem_limit;

  if (pthread_create(watchdog_thread, NULL, mem_watchdog, arg_thread))
  {
    writes("Could not create watchdog thread\n");
    exit(3);
  }
}

// Provide the uid/gid of a user
void get_uid(char* login, uid_t* uid, gid_t* gid)
{
  struct passwd* pwd;

  pwd = getpwnam(login);
  ASSERT(pwd != NULL, "cannot get ID of the sandboxBigBrother user");

  *uid = pwd->pw_uid;
  *gid = pwd->pw_gid;
}

// Client's code user infos
void get_client_username(char* username)
{
  int num;

  srand(time(NULL) + getpid());
  num = (rand() % CLIENT_USER_MAX) + 1;
  snprintf(username, 100, CLIENT_USER_FMT, num);
}

// Entry point
int main(int argc, char** argv, UNUSED char** envp)
{
  // Parameters
  if (argc < 4)
  {
    fprintf(stderr,
        "usage: %s swap[on|off] timeout mem_limit program [arguments...]\n",
        argv[0]);
    exit(EXIT_FAILURE);
  }

  uid_t cl_uid;
  gid_t cl_gid;
  char cl_username[100];
  get_client_username(cl_username);
  get_uid(cl_username, &cl_uid, &cl_gid);

  // Chroot and privilege dropping
  ASSERT(chdir(dirname(argv[0])) == 0, "cannot chdir");
  ASSERT(chroot("../..") == 0, "cannot chroot");

  // Paramters
  pid_t child_pid = -1;
  int mem_limit = atoi(argv[3]) * 1024; // in Bytes
  int swapoff = !strcmp(argv[1], "swapoff");

  // Fork/Exec
  if (!(child_pid = fork()))
  {
    ASSERT(drop_privs(cl_uid, cl_gid) == 0, "cannot drop privileges");
    child_exec(swapoff, argv + 4);
  }
  // Monitor
  else
  {
    close(STDIN_FILENO);
    ASSERT(drop_privs(cl_uid, cl_gid) == 0, "cannot drop privileges");

    int time_limit = atoi(argv[2]) * 1000; // in micro-sec
    pthread_t watchdog_thread = 0;

    run_mem_watchdog(child_pid, mem_limit, &watchdog_thread);

    return monitor_time(child_pid, &watchdog_thread,
            time_limit, mem_limit);
  }

  return (EXIT_SUCCESS);
}
