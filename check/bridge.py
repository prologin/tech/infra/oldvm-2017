#!/usr/bin/env python

import json
import os
import sys
import tempfile
import tornado.gen
import tornado.ioloop
import tornado.process
import tornado.web

PREFIX = '/var/prologin'
RUNSH = os.path.join(PREFIX, 'check', 'run.sh')
PACKAGES = tuple(filter(bool, map(lambda x: x.strip(),
    open(os.path.join(PREFIX, 'deps', 'sandbox')).read().splitlines())))


@tornado.gen.coroutine
def packages_versions(package_list):
    args = ('pacman', '-Qi') + tuple(package_list)
    p = tornado.process.Subprocess(args,
            stdout=tornado.process.Subprocess.STREAM)
    r = yield tornado.gen.Task(p.stdout.read_until_close)
    del p

    r = r.decode()
    d = {}
    cname = None
    for l in r.splitlines():
        try:
            k, v = map(lambda x: x.strip(), l.split(':', 1))
        except ValueError:
            continue
        if k == 'Name':
            cname = v
        if cname and k == 'Version':
            d[cname] = v
            cname = None
    return d


class BridgeHandler(tornado.web.RequestHandler):
    def decode_argument(self, value, name=None):
        return value.decode('latin1')

    def get(self):
        self.set_header('Content-Type', 'text/plain; charset=utf-8')
        self.write("Bridge working.\n")

    @tornado.gen.coroutine
    def post(self):
        self.set_header('Content-Type', 'text/plain; charset=utf-8')
        try:
            challenge = self.get_argument('challenge')
            problem = self.get_argument('problem')
            source = self.get_argument('source')
            filename = self.get_argument('filename')
        except tornado.web.MissingArgumentError:
            self.write("Error, missing fields!")

        ext = filename.split('.')[-1]

        with tempfile.NamedTemporaryFile(suffix='.' + ext) as tmpf:
            tmpf.write(source.encode('utf-8'))
            tmpf.flush()

            args = [RUNSH, challenge, problem, tmpf.name]
            p = tornado.process.Subprocess(args,
                    stdout=tornado.process.Subprocess.STREAM)
            r = yield tornado.gen.Task(p.stdout.read_until_close)
            del p

            self.write(r)


class VersionsHandler(tornado.web.RequestHandler):
    @tornado.gen.coroutine
    def get(self):
        self.set_header('Content-Type', 'text/plain')
        d = yield packages_versions(PACKAGES)
        self.write(json.dumps(d, indent=2))


application = tornado.web.Application([
    (r"/", BridgeHandler),
    (r"/submit", BridgeHandler),
    (r"/versions", VersionsHandler),
])

if __name__ == "__main__":
    port = int(sys.argv[1]) if len(sys.argv) > 1 else 80
    application.listen(port)
    tornado.ioloop.IOLoop.instance().start()
